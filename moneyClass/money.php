<?php
/*
 * A Simple Money Class for handling popular money operations
 * @oprosper
 * @author evi prosper Onogberie
 */
 
if(!class_exists('pdoconf.php')){ include_once('pdoconf.php'); }


 class Money{   
    /**
     * Rate of VAT- to be dertermined by user country laws
     * @var float
     */
    private $vat;
    
    /**
     * Currency symbol
     * @var string
     */
    private $symbol;
    
    /**
     * Total
     * @var float 
     */
    private $total;
    
    /**
     * Total VAT
     * @var float
     */
    private $totalVat;
    
    /*
     * Total of products with VAT
     * @var float
     */
    private $totalProductsWithVat;
    
    /**
     * Total VAT from products with VAT
     * @var float
     */
    private $totalProductsWithVatVat;
    
    /**
     * Total products without VAT
     * @var float
     */
    private $totalProductsWithoutVat;
    
    
    /**
     * Currency Table
     * Stores relevant currency information
     */
    private $currencyTable = 'currency';
	
	/**
     * Exchange rates Table
     * Stores relevant currency exchange rates
     */
    private $exchangeRatesTable = 'exchange_rates';
    
    /**
     * Constructor
     * SETs the rate of VAT and Symbol
     */
    public function __construct() 
    {
        #set VAT this will need to be pulled from the merchant set location
        $this->vat = 0.5;
        #Set currency this will need to be pulled from user set currency
        #$this->symbol = "$"; 
        #Set totals
        $this->total = 0.00;
        $this->totalVat = 0.00;
        $this->totalProductsWithVat = 0.00;
        $this->totalProductsWithVatVat = 0.00;
        $this->totalProductsWithoutVat = 0.00;
    }
    
    
	
	
	
	/**
	 * Get list of Supported Currencies
	 * Returns the associative array of list of supported currencies
	 * @return array currencies
	 */
	 public function getCurrencies()
	 {
			$dbcon = pdo_dbcon();
			$base = 0;
			$stmt = $dbcon->prepare(" SELECT * FROM $this->currencyTable ");
			#var_dump(0);
			#exit;
        	$stmt->execute();
        	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        	#return $rows[0]['htmltag'];
			return $rows; 
	 }
	 
	 /**
     * Get currency HTML Code
     * Returns the html code of given currency code.
     * @param string currencycode international currency code
     * @return string currency html code
     */
	 public function getCurrencyHtmlCode( $currencyCode )
	 {
		$currencies = $this->getCurrencies(); 
		foreach($currencies as $curr)
		{
			$currCode = $curr['currencycode'];
			$htmlcode = $curr['htmltag'];
			if($currCode == $currencyCode){ $rightCode = $htmlcode; }
		}
		return $rightCode;
	 }
	 
	 /**
	  * Returns the last received exchange rates
	  * @return type array rates
	  */
	 public function getExchangeRates()
	 {
		 	$dbcon = pdo_dbcon();
			$stmt = $dbcon->prepare(" SELECT * FROM $this->exchangeRatesTable ORDER BY entrydate DESC LIMIT 1 ");
			$stmt->execute();
        	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        	return $rows; 
	 }
	 
	 /**
	  * Returns value of given amount in given currency based on last available exchange rate
	  * @param type string currencycode
	  * @param type float base_currency_amount
	  * @return type float new amount value
	  */
	  public function getExchangeValue( $currencyCode, $BaseCurrencyAmount )
	  {
			 $rates = $this->getExchangeRates();
			 $rateValue = $rates[0][$currencyCode];
			 $exchangeValue = $rateValue*$BaseCurrencyAmount;
			 return $exchangeValue;
	  }
    
    /**
     * Format to currency
     * Rounds a number to 2 decimal places
     * Formats the number to two decimal places
     * 
     * @param float Number to be formatted 
     * @return float Formatted number
     */
    public function format( $input ){
        #Round number and format to two decimal places
        return number_format( round( $input, 2 ), 2 );
    }
    
    /**
     * VAT
     * Calculate s and returns the formatted VAT
     * @param float Price to calculate VAT from
     * @return float VAT amount
     */
    public function vat( $input )
    {
        #Return formatted Vat from input amount
        return $this->format( $input * $this->vat );
    }
    
    /**
     * Add
     * Add to running total
     * if VAT is set, get VAT and add to total
     * Else, just add to total
     * return the amount of VAT
     * 
     * @param float the Price of the item
     * @param bool if VAT is set
     * @return foat the amount of VAT
     */
    public function add( $input, $vat = NULL )
    {
        #if VAT is set
        if( $vat == true )
        {
            #Get the VAT
            $vat = $this->vat( $input );
            #Add to running total
            $this->total += ( $input + $vat );            #Add to running VAT total
            $this->totalVat += $vat;
            #Add to products with VAT total
            $this->totalProductsWithVat += $input;
            #Add VAT to products with VAT total
            $this->totalProductsWithVatVat += $vat;
            #return the amount of vat
            return $vat;
        }else{
            #Add to running total
            $this->total += $input;
            #Add to products without VAT total
            $this->totalProductsWithoutVat += $input;
            #return 0.00 Vat
            return $this->format(0.00);
        }
    }
    
    
    /**
     * Display
     * Display the amount
     * Add VAT if applicable
     * Add currency symbol
     * Return formatted
     * 
     * @param float the price of the item
     * @param bool if VAT is set
     * @return float the formatted price
     */
    public function display( $input, $vat = NULL )
    {
        #if Vat is set
        if( $vat == true )
        {
            #Get Vat
            $vat = $this->vat( $input );
            #add VAT to input
            $input += $vat;
            #retur the formatted total
            return $this->symbol . $this->format( $input );
        }else{
            #return formatted
            return $this->symbol . $this->format( $input );
        }
    }
    
    /**
     * Display VAT
     * Display the rate of VAT as a percentage
     * 
     * @return strin e.g "20%";
     */
    public function displayVat()
    {
        return ( $this->vat * 100 ). "%";
    }
    
    /**
     * Percentage Savings
     * Returbs the total percentage savings from given price difference
     * 
     * @param float Regular price
     * @param float Discount price
     * @return string e.g "5% saved"
     */
    public function savingsPercent( $highPrice, $lowPrice )
    {
         # x% = 100 - ($lowPrice/($highPrice/100));
        $percerntSavings = 100 - ( $lowPrice / ( $highPrice / 100 ) );
        return $percentSavings . "% Saved";
    }
    
    /**
     * Total
     * Return the running total as either a float or a formatted string
     * 
     * @param string Type of formatting
     * @return float
     */
    public function total( $type = NULL )
    {
        #Return running total
        if( $type == "string" )
        {
            #Return formatted as a string
            return $this->symbol . $this->format( $this->total );
        }else{
            #Return as float
            return $this->format( $this->total );
        }
    }
    
    /**
     * Total VAT
     * Return the running VAT total as either a float or a formatted string
     * 
     * @param striing type of formatting
     * @return float
     */
    public function totalVat( $type = NULL )
    {
        #return running VAT total
        if( $type == "string" )
        {
            #return formatted as a string
            return $this->symbol . $this->format($this->totalVat);
        }else{
            #Return as float
            return $this->format($this->totalVat);
        }
    }
    
    /**
     * Total minu VAT
     * Return the total minus VAT
     * 
     * @param string Type of formatting
     * @return float
     */
    public function totalMinusVat( $type = NULL )
    {
        #Return total minus VAT
        if( $type == "string" )
        {
            #Return formatted as a string
            return $this->symbol . $this->format( $this->total - $this->totalVat );
        }else{
            #Return as float
            return $this->format( $this->total - $this->totalVat );
        }
    }
    
    /**
     * Total of products with VAT
     * Returns a running total of the value of the products with VAT
     * 
     * @param string Type of formatting
     * @return string as string
     * @return float as float
     */
    public function totalProductsWithVat( $type = NULL )
    {
        if( $type == "string" )
        {
            #return formatted total products with VAT
            return $this->symbol . $this->format( $this->totalProductsWithVat );
        }else{
            #Return float
            return $this->format( $this->totalProductsWithVat );
        }
    }
    
    /**
     * Total of products without VAT
     * Returns a running total of the value of products without VAT
     * 
     * @param string Type of formatting
     * @return string as String
     * @return float as float
     */
    public function totalProductsWithoutVat( $type = NULL )
    {
        if( $type == "string" )
        {
            #return formatted total products without VAT
            return $this->symbol . $this->format( $this->totalProductsWithoutVat );
        }else{
            #return float
            return $this->format( $this->totalProductsWithoutVat);
        }
    }
    
    /**
     * CurrrencyCodeToSymbol
     * Returns the stored html currency symbol of the given currency code
     * @param string  $currencyCode International representation of currency code
     * @return string html currency symbol
     */
    public function currencyCodeToSymbol( $currencyCode )
    {        
        
        $dbcon = pdo_dbcon();
        $stmt = $dbcon->prepare(" SELECT currencycode, htmltag FROM $this->currencyTable WHERE currencycode=:code ");
        $stmt->execute(array(':code' => $currencyCode ));
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows[0]['htmltag'];
    }
}
