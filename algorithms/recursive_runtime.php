<?php

//testing recursion for O(N)
function f( $n ){
    if($n <= 1){
        return 1;
    }
    return f($n-1) + f($n-1);
}

(int)$n = 8;
echo f($n);
