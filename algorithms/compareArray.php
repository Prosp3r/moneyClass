<?php
// Complete the solve function below.
function solve($a, $b) {
    $alice = 0;
    $bob = 0;
    for( $x = 0; $x < count($a); $x++ )
    {
        if( $a[$x] > $b[$x] )
        {
            $alice++;
        }else if( $b[$x] > $a[$x] )
        {
            $bob++;
        }else{
            //do nothing  $b[$x] == $a[$x]
        }
    }
    //return both values
    return array($alice, $bob);
}
