<?php

/**Returns specified number of white spaces*/
function spaceIt( $number )
{
    $spacer = "";
    for( $x = 0; $x < $number; $x++ )
    {
        $spacer .= " ";
    }
    return $spacer;
}

function bringHashes( $number )
{
    $hashes = "";
    for( $x = 0; $x < $number; $x++ )
    {
        $hashes .= "#";
    }
    return $hashes;
}
    
    
// Complete the staircase function below.
function staircase($n) {
    $line = "#";
    $spaces = 0;
    for( $x = 1; $x <= $n; $x++ )
    {
        $spaces = spaceIt( $n - $x );
        $hashes = bringHashes( $x );
        $tower .= $spaces.$hashes.PHP_EOL;
    }
    echo $tower;

}

staircase(6);
