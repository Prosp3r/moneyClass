<?php
//My second solution to HackerRanks Grading round off
//impementation isolates all major activity to the auxilary function roundOff and implements it with the array_map function
//actually look more elegant...should test for eficiency
//THIS IS RUMOURED TO BE HIGHLY INNEFFICIENT MORE SO THAN IT'S FOREACH CAUSIN :(
function roundOff( $MyGrade ){
    //round off to the nearest 36
    $lower_limit = 34;
    $gradeDiv = $MyGrade/5; //7
    $remainder = $MyGrade%5; //1
    $toAdd =  ((($gradeDiv*5) + 5 ) - $remainder) - $MyGrade;
    #var_dump($toAdd);
    if( $MyGrade > $lower_limit ){
        if($remainder >= 3){
            $MyGrade = $MyGrade + $toAdd;
            return $MyGrade;
        }else{
             //$MyGrade =
            return $MyGrade;
        }
    }else{
        return $MyGrade;
    }
}
//
function gradingStudents( $grades ){
    $result = array_map("roundOff", $grades);
    return $result;
}

$grades = array(73, 67, 38, 33);

//TEST CASE 10
#    $grades = array(36, 6, 98, 25, 97, 24, 25, 70, 50, 71, 30, 14, 28, 100, 3, 26, 61, 98, 50, 41, 5, 3, 28, 34, 0);
/*EXPECTED OUTPUT   36  6  100 25  97  24  25  70  50  71  30  14  28  100  3  26  61  100 50  41  5  3  28  34  0
*/


//TEST CASE 9
#  $grades = array(86, 30, 0, 16, 51, 53, 42, 48, 22, 69, 12, 27, 34, 24, 95, 16, 32, 22, 52, 56, 71, 95);
/*EXPECTED OUTPUT[86  30  0  16  51  55  42  50  22  70  12  27  34  24  95  16  32  22  52  56  71  95 ]*/


var_dump( gradingStudents($grades));
