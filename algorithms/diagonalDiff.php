<?php
// Complete the diagonalDifference function below.
function diagonalDifference($arr) {
    
        #var_dump($arr);
        #exit;
        $y = ( count($arr) - 1 );
        $right = array();
        $left = array();
        for( $x = 0; $x < count($arr); $x++ )
        {
            //$x is the number of rows
			
            $left[$x] = $arr[$x][$x];
            $right[$x] = $arr[$x][$y];
            $y--;
//	    echo "left: $left[$x], right: $right[$x] \n";
        }
        $total_right = array_sum($right);
        $total_left = array_sum($left);
        
        $diff = $total_left - $total_right;
	//convert to positive integer
	if( $diff < 0 ){ $diff = $diff*-1; }
        return $diff;
}
$arr1 = array(12, 20, -4);
$arr2 = array(4, 50, 5);
$arr3 = array(11, 8, 10);
$first_arr = array($arr1, $arr2, $arr3);

$result = diagonalDifference($first_arr);
echo $result;
