<?php

/*
 * Complete the timeConversion function below.
 */
function timeConversion($s) {
    /*
     * Write your code here.
     */
    $time = $s;
    $ampm = substr($time, -2);
    
        if( strtolower($ampm) == 'am' ) //it's in AM
        {
            $hour = substr($time, 0, 2);
            $mins = substr($time, 3, 2);
            $secs = substr($time, 6, 2);
		if( (int)$hour == 12 ){
			$hour = 00;
		}
	    $hour = sprintf('%02d', $hour);
            $mins = sprintf('%02d', $mins);
            $secs = sprintf('%02d', $secs);
            
	    $milTime = "$hour:$mins:$secs";
        }
    
        if( strtolower($ampm) == 'pm' ) //it's in PM
        {
            $hour = substr($time, 0, 2);
            $mins = substr($time, 3, 2);
            $secs = substr($time, 6, 2);
	    #var_dump($hour);
	    #exit;
            if( (int)$hour != 12 ){
		$hour = $hour + 12;
	    }
	    //echo sprintf('%02d', $key); // outputs 04

            $hour = sprintf('%02d', $hour);
	    $mins = sprintf('%02d', $mins);
	    $secs = sprintf('%02d', $secs);

            $milTime = "$hour:$mins:$secs";
        }
    return $milTime;

}
$s = "07:05:45PM";
#$s = "12:00:00AM";
$result = timeConversion($s);
echo $result;




/**
Given a time in 12-hour AM/PM format, convert it to military (24-hour) time.

Note: Midnight is 12:00:00AM on a 12-hour clock, and 00:00:00 on a 24-hour clock. Noon is 12:00:00PM on a 12-hour clock, and 12:00:00 on a 24-hour clock.

Input Format

A single string containing a time in -hour clock format (i.e.hh:mm:ssAM or hh:mm:ssPM ), where 01<=hh<=12 and 00<=mm, ss<=59.

Output Format

Convert and print the given time in 24-hour format, where 00<=hh<=23.

Sample Input 0

07:05:45PM

Sample Output 0

19:05:45
*/
