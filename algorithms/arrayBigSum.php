<?php

// Complete the aVeryBigSum function below.
function aVeryBigSum($ar) 
{
    $ans = array_sum($ar);
    return $ans;
}

$ar = array(1000000001, 1000000002, 1000000003, 1000000004, 1000000005);
$result = aVeryBigSum($ar);
echo $result;
