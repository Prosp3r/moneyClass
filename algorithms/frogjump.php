<?php

function solution( $X, $Y, $D ){
    $counter = 0;
    $counter = (int)ceil(($Y-$X)/$D);
    return $counter;
}

$X = 10;
$Y = 85;
$D = 30;
echo solution($X, $Y, $D);
