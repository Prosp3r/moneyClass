<?php

//My second take on that binary gap problem
function solution($A){
    $ar_bin = bindec($A);
    $ar_bin = str_split($ar_bin);
    $store_gaps = array();
    $count_ar = -1;
    foreach( $ar_bin as $ar ){
        //
        if( $ar == "1" ){
            $count_ar += 1;
            $gaps = 0; //reset zero counts
        }else{
            //count zeros
            $gaps++;
            $store_gaps[$count_ar] = $gaps;
        }
    }
    return max($store_gaps);
}
#$ar = "10010";
#$a = 1041;
$a = array(1041, 24, 32, 147);
foreach( $a as $i ){
    $bin = bindec($i);
    echo "Out put for $i => binary => $bin => ans: ";
    echo solution($i);
    echo PHP_EOL;
}
;
