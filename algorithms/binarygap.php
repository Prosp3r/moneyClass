<?php

function solution( $str ){
	$strray = str_split($str);
	$zercount = array();
	$wancount = 0;
	$str_l = strlen($str);

	for( $x = 0; $x < $str_l; $x++ ){
		//Check if there's a one behind the rest of the chracters numbers
		$hasoneafter = in_array("1", array_slice($strray, $x+2));
		if( $strray[$x] == "1" ){
			$wancount++; //count the ones
			if( $hasoneafter == false){
					//var_dump(array_search("1", array_slice($strray, $x+2)));
					break;//stop counting/adding this key because it ends in zeros
			}
		}else{
			//Zero key creation and increments
			if( $zercount[$wancount] > 0 ){
				$zercount[$wancount] += 1; //count the zeros of the current ones set
			}else{
				$zercount[$wancount] = 1; //count the zeros of the current ones set
			}//end if
		}//end if
	}
	if( count($zercount) > 0 ){
		return max($zercount);
	}else{
		return 0;
	}
}


#$str = "10101000";
#echo solution($str);
#$num = 2147483647;
$numray = array(805306373, 1376796946, 9, 42, 805306373, 15, 16, 1040, 32, 2147483647, 202);
foreach($numray as $num){
	$str = decbin( (int)$num );
	echo "Processing for number $num binary=> $str =>: ";
	echo solution($str);
	echo PHP_EOL;

}
