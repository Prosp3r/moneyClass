<?php

function solution($A){
    #$arr_size = count($A);
    #$lowest_in_array = min($A);
    #$highest_in_array = max($A);
    $missing = 0;

    if( count($A) < 1 ){
        #$next = 1;
        return 1;
    }


    for( $x = min($A); $x < max($A); $x++ ){
        $next = $x+1;
        if( !(in_array($next, $A, true) )){
            //it's there
            return $next;
        }
    }
        return ((max($A))+1);
}
#$A = array(7, 9, 6, 5);
$A = array(2, 3, 4, 5);
#$A = array();
echo solution($A);
