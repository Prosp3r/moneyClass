<?php

// Complete the miniMaxSum function below.
function miniMaxSum($arr) 
{
    // $arr = array( 1, 2, 3, 4, 5);
    #var_dump( array_splice($arr, 1, 1) );
    #unset($arr[1]);
    #var_dump($arr);
    #exit;

    $resultArray = array();
    for( $x = 0; $x < count($arr); $x++ )
    {
        $new_arr = $arr;
	unset($new_arr[$x]);
        $resultArray[$x] = array_sum( $new_arr );
    }
    $lowestVal = min( $resultArray );
    $highestVal = max( $resultArray );
    #return array($lowestVal, $highestVal);
	echo $lowestVal." ".$highestVal;
}

$ar = array(1, 2, 3, 4, 5);
$result = miniMaxSum($ar);
echo $result;
/*
$stdin = fopen("php://stdin", "r");

fscanf($stdin, "%[^\n]", $arr_temp);

$arr = array_map('intval', preg_split('/ /', $arr_temp, -1, PREG_SPLIT_NO_EMPTY));

miniMaxSum($arr);

fclose($stdin);
*/

/**
Given five positive integers, find the minimum and maximum values that can be calculated by summing exactly four of the five integers. Then print the respective minimum and maximum values as a single line of two space-separated long integers.

Input Format

A single line of five space-separated integers.

Constraints

    Each integer is in the inclusive range .

Output Format

Print two space-separated long integers denoting the respective minimum and maximum values that can be calculated by summing exactly four of the five integers. (The output can be greater than a 32 bit integer.)

Sample Input

1 2 3 4 5

Sample Output

10 14

Explanation

Our initial numbers are 1, 2, 3, 4, and 5. We can calculate the following sums using four of the five integers:

    If we sum everything except 1, our sum is 2 + 3 + 4 + 5 = 14. 
    If we sum everything except 2, our sum is 1 + 3 + 4 + 5 = 13.
    If we sum everything except 3, our sum is 1 + 2 + 4 + 5 = 12.
    If we sum everything except 4, our sum is 1 + 2 + 3 + 5 = 11.
    If we sum everything except 5, our sum is 1 + 2 + 3 + 4 = 10.
*/

