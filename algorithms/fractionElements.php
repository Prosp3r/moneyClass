<?php

// Complete the plusMinus function below.
function plusMinus($arr) {
    
    #define +ve and -ve counts
    $positive_count = 0;
    $positive_sum = 0;
    $negative_count = 0;
    $negative_sum = 0;
    $zero_count = 0;
    $counter = 0;
    
    $positive_fraction = 0;
    $negative_fraction = 0;
    $zero_fraction = 0;
    
    foreach( $arr as $ar )
    {
        if( $ar > 0 )
        {
            //is positive
            $positive_count++;
            $positive_sum += $ar; 
        }else if( $ar < 0 ){
            //is nevgative
            $negative_count++;
            $negative_sum += $ar;
        }else{
            //is zero
            $zero_count++;
        }
        $counter++;
    }
    
    $total_count = count( $arr );
    if($counter > 0)
    {
        $positive_fraction = number_format( ($positive_count/$counter), 6 );
        $negative_fraction = number_format( ($negative_count/$counter), 6 );
        $zero_fraction     = number_format( ($zero_count/$counter), 6 );
        
        #return array($positive_fraction, $negative_fraction, $zero_fraction);
        echo $positive_fraction.PHP_EOL.$negative_fraction.PHP_EOL.$zero_fraction.PHP_EOL;    
    }
}

$stdin = fopen("php://stdin", "r");

fscanf($stdin, "%d\n", $n);

fscanf($stdin, "%[^\n]", $arr_temp);

$arr = array_map('intval', preg_split('/ /', $arr_temp, -1, PREG_SPLIT_NO_EMPTY));

plusMinus($arr);

fclose($stdin);
