<?php

/*
 * Complete the chocolateInBox function below.
* https://www.youtube.com/watch?v=ORaGSyewF9Q
 */
    

/**
*Returns the difference between array elements of a two element array
*/
function diffMe( $array_element )
{
    //
    if( $array_element[0] > $array_element[1])
    {
        $diff = $array_element[0] - $array_element[1];
    }else{
        $diff = $array_element[1] - $array_element[0];
    }
        return $diff;
}
    
function chocolateInBox($arr) {
    /*
     * Write your code here.
     * Trick is to make the first move in such a way that both piles are even  so that
     * when second player makes a pick it always ends in two piles of which sumation results in an           
     * odd number of candy left.
     * e.g. [7 4] > [ 4 4 ] > (second player has no choice but to take from one pile)
     * but whatever number of candy she picks player one can pick to be last player to pick
     * e.g. [4 4] > [ 4 3 ] > [ 3 3 ] > or
     *      [4 4] > [ 4 2 ] > [ 2 2 ] > ;) first player always evens out the piles 
     * So the question is basically howmany moves can even out the piles at first touch ? ;)
     */
    
    $box = array();
    //$player_names = array('dexter', 'debra');
    //$minCandy = min( $arr );
    //$maxCandy = max( $arr );
    //$players = count( $player_names );
    
    for( $x = 0; $x < count($arr); $x++ )
    {
        $box[$x] = $arr[$x];
    }
    
  	//Check if the first move evens out the candy box    
        $element_diff = diffMe( $box );
        if( $element_diff > 0 )
        { 
            //The first move should be 1 else 0
            $possible = 1;
        }else{
            $possible = 0;
        }
	return $possible;
}

$arr = array(2, 3);
$result = chocolateInBox($arr);
echo $result;
