#!/bin/bash
# Change the delimiter of a list of contacts from newline or caiage returns to comma
# Prosper
tr new_prospect_database.csv '\n' ',' < new_prospect_database.csv > new_prospect_database_delimited.csv

exit
